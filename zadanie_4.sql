--1.

CREATE DATABASE Lista_Prezentów;
USE Lista_Prezentów;


--2,3.

CREATE TABLE Prezenty_2020 (
    id INTEGER UNIQUE NOT NULL,
    imie_obdarowanego TEXT NOT NULL,
    pomysl_na_prezent TEXT NOT NULL,
    cena FLOAT DEFAULT 0,
    miejsce_zakupu TEXT DEFAULT 'NOT APPLICABLE'
);

--4. 

INSERT INTO Prezenty_2020 (id, imie_obdarowanego, pomysl_na_prezent, cena, miejsce_zakupu)
VALUES (1, 'Anna', 'portfel', 50, 'allegro'),
       (2, 'Ewa', 'naszyjnik', 100, 'apart'),
       (3, 'Piotr', 'hulajnoga', 300, 'martes'),
       (4, 'Kamil', 'skarpety', 30, 'adidas'),
       (5, 'Maja', 'perfumy', 150, 'douglas');

--5.

SELECT * FROM Prezenty_2020;

--6. 

UPDATE Prezenty_2020
SET pomysl_na_prezent = 'rower'
WHERE id = 3;

--7.

SELECT * 
FROM Prezenty_2020
WHERE id = 3;

--8.

DELETE FROM Prezenty_2020
WHERE id = 1;

--9.

SELECT * FROM Prezenty_2020;

--10.

ALTER TABLE Prezenty_2020 DROP COLUMN miejsce_zakupu;

--11.

SELECT imie_obdarowanego
FROM Prezenty_2020
WHERE id BETWEEN 3 AND 5;

SELECT * FROM Prezenty_2020;
