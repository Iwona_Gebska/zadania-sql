--1.

CREATE DATABASE iwona;
USE iwona;

--2.

CREATE TABLE biblioteczka (
    id INTIGER UNIQUE NOT NULL,
    tytuł TEXT,
    data_zakupu DATE
);

--3.

INSERT INTO biblioteczka (id, tytuł, data_zakupu)
values (1, 'potop', '2020-02-02');

--4.

SELECT * FROM biblioteczka;

--5.

INSERT INTO biblioteczka (id, tytuł, data_zakupu)
VALUES (2, 'tajemnica', '2020-03-03'),
       (3, 'zmierzch', '2019-09-09');

--6.

UPDATE biblioteczka
SET tytuł = '365 dni'
WHERE id = 3;

UPDATE biblioteczka
SET data_zakupu = '2018-03-09'
WHERE id = 3;

SELECT * FROM biblioteczka;

--7.

ALTER TABLE biblioteczka ADD Autor TEXT;

--8.

UPDATE biblioteczka
SET Autor = 'Henryk Sienkiewicz'
WHERE id = 1;

UPDATE biblioteczka 
SET Autor = 'Jan Nowak'
WHERE id = 2;

UPDATE biblioteczka
SET Autor = 'Anna Kowalska'
WHERE id = 3;

--9.

DELETE FROM biblioteczka
WHERE id = 1;

DELETE FROM biblioteczka
WHERE id = 2;

--10.

SELECT * FROM biblioteczka;





