--1.

CREATE DATABASE School_Coding;
USE School_Coding;

--2,3.

CREATE TABLE Mentors (
    id INTIGER UNIQUE NOT NULL,
    Imie TEXT NOT NULL,
    Nazwisko TEXT NOT NULL,
    Specjalizacja TEXT DEFAULT 'NOT APPLICABLE',
    Data_zatrudnienia DATE CHECK(Data_zatrudnienia < '2021-03-12'),
    Data_zwolnienia DATE CHECK(Data_zwolnienia < '2021-03-12'),
);

--4.

INSERT INTO Mentors (id, Imie, Nazwisko, Specjalizacja, Data_zatrudnienia, Data_zwolnienia)
VALUES (1, 'Anna', 'Nowak', 'Java', '2018-03-05', '2020-09-09'),
       (2, 'Ewa', 'Gil', 'Java', '2018-03-05', '2020-09-09'),
       (3, 'Alicja', 'Nowak', 'Java', '2018-03-05', '2020-09-09'),
       (4, 'Natalia', 'Lis', 'Java', '2018-03-05', '2020-09-09'),
       (5, 'Tomasz', 'Knawa', 'Java', '2018-03-05', '2020-09-09'),
       (6, 'Maja', 'Kowal', 'Java', '2018-03-07', '2020-07-02'),
       (7, 'Piotr', 'Nowak', 'Java', '2019-08-07', '2020-09-03'),
       (8, 'Paweł', 'Fryc', 'Java', '2018-04-03', '2019-10-10'),
       (9, 'Magda', 'Bela', 'Java', '2107-10-10', '2016-07-09'),
       (10, 'Kamil', 'Lis', 'Java', '2016-02-02', '2020-08-09');

--5.

SELECT * FROM Mentors;

--6.

UPDATE Mentors
SET Nazwisko = 'Krupa'
WHERE id = 5;

--7.

SELECT * FROM Mentors
WHERE id = 5;

--8.

UPDATE Mentors
SET Specjalizacja = 'Phyton'
WHERE id = 9;

--9.

SELECT *
FROM Mentors
WHERE id = 9;

--10.

ALTER TABLE Mentors ADD Wynagrodzenia FLOAT;

--11. 

UPDATE Mentors
SET Wynagrodzenia = 1000
WHERE id = 1;

UPDATE Mentors
SET Wynagrodzenia = 1000
WHERE id = 2;

UPDATE Mentors
SET Wynagrodzenia = 100
WHERE id = 3;

--12.

SELECT *
FROM Mentors
WHERE Wynagrodzenia = 1000;





